var express = require('express');
var serveStatic = require('serve-static');
var app = express();

app.use(serveStatic('public_html', {'index': ['index.html', 'index.htm']}));
app.use(function(req, res, next) {
  res.status(404).sendFile('public_html/404.html');
});;
app.listen(1337);
