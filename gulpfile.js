var gulp = require('gulp');
var gutil = require('gulp-util');
var notify = require('gulp-notify');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var fileinclude = require('gulp-file-include');
var livereload = require('gulp-livereload');
var lr = require('tiny-lr');
var server = lr();

gulp.task('styles',  function() {
  return gulp.src('./src/sass/*.scss')
  .pipe(sass({
    outputStyle: 'compressed'
  }))
  .on('error', function() {
    gutil.log(gutil.colors.red('## FIX THE SCSS! ###'));
    gutil.beep();
  })
  .pipe(postcss([
    autoprefixer({browsers: ['last 2 versions']})
  ]))
  .pipe(gulp.dest('./public_html/css'))
  .pipe(livereload(server));
});

gulp.task('scripts', function() {
    return gulp.src([
      './src/js/script.js',
      './src/js/*.js'
    ])
    .pipe(concat('scripts.js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('./public_html/js'))
    .pipe(livereload(server))
    .pipe(notify({ message: 'Scripts task complete' }));
});

gulp.task('html', function(){
  return gulp.src('./public_html/*.html')
    .pipe(livereload(server));
});

gulp.task('fileinclude', function() {
  gulp.src(['./src/index.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('./public_html'));
});

//Default
gulp.task('default', function() {
    gulp.start('styles', 'scripts');
});

// Watch
gulp.task('watch', function() {
  // Livereload listen on default port 35729
  server.listen(35729, function (err) {
    if (err) {
      return console.log(err);
    }
    gulp.watch('./src/sass/**/*.scss', ['styles']);
    gulp.watch('./src/js/**/*.js', ['scripts']);
    gulp.watch('./public_html/*.html', ['html']);
    gulp.watch('./src/partials/*.html', ['fileinclude']);
  });
});
